import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import moment from 'moment';
import SimpleSchema from 'simpl-schema';

export const Chaufs = new Mongo.Collection('chaufs');

if (Meteor.isServer) {
    Meteor.publish('chaufs', function() {
        return Chaufs.find({userId: this.userId});
    });
}

Meteor.methods({
    'chaufs.insert'() {
        if(!this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        console.log('added new chauf');
        return Chaufs.insert({
            name: '',
            code: '',
            tourLength: '',
            returnTime: '',
            working: false,
            startTime: '',
            userId: this.userId,
            onStation: true
        })
    },
    'chaufs.remove'(_id) {
        if(!this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        new SimpleSchema({
            _id: {
                type: String,
                min: 1
            },
        }).validate({_id})
        console.log('removed chauf, id: ', _id);
        Chaufs.remove({ _id, userId: this.userId });        
    },
    'chaufs.update'(_id, updates){
        if(!this.userId) {
            throw new Meteor.Error('not-authorized');
        }
        
        new SimpleSchema({
            _id: {
                type: String,
                min: 1
            },
            name: {
                type: String,
                optional: true
            },
            code: {
                type: String,
                optional: true
            },
            tourLength: {
                type: Number,
                optional: true
            },
            working: {
                type: Boolean,
                optional: true
            },
            startTime: {
                type: String,
                optional: true
            },
            returnTime: {
                type: String,
                optional: true
            },
            onStation: {
                type: Boolean,
                optional: true
            }
        }).validate({
            _id,
            ...updates
        });
        console.log('chauf updated, id: ', _id);
        Chaufs.update({
            _id,
            userId: this.userId
        }, {
            $set: {
                ...updates
            }
        })
    },
    'chaufs.resetWork'(){
        Chaufs.update({}, {$set: { working: false}}, { multi: true });
    }
})

