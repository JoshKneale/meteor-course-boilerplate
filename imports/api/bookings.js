import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import moment from 'moment';
import SimpleSchema from 'simpl-schema';

export const Bookings = new Mongo.Collection('bookings');

if (Meteor.isServer) {
    Meteor.publish('bookings', function() {
        return Bookings.find({userId: this.userId});
    });
}

Meteor.methods({
    'bookings.insert'() {
        if(!this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        console.log('added new booking');
        return Bookings.insert({
            startTime: '',
            tourLength: 0,
            name: '',
            numberOfPeople: 0,
            userId: this.userId
        })
    },
    'bookings.remove'(_id) {
        if(!this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        new SimpleSchema({
            _id: {
                type: String,
                min: 1
            },
        }).validate({_id})
        console.log('removed booking, id: ', _id);
        Bookings.remove({ _id, userId: this.userId });        
    },
    'bookings.update'(_id, updates){
        if(!this.userId) {
            throw new Meteor.Error('not-authorized');
        }
        
        new SimpleSchema({
            _id: {
                type: String,
                min: 1
            },
            startTime: {
                type: String,
                optional: true
            },
            tourLength: {
                type: Number,
                optional: true
            },
            name: {
                type: String,
                optional: true
            },
            numberOfPeople: {
                type: Number,
                optional: true
            }
        }).validate({
            _id,
            ...updates
        });
        console.log('booking updated, id: ', _id);
        Bookings.update({
            _id,
            userId: this.userId
        }, {
            $set: {
                ...updates
            }
        })
    }
})

