import React from 'react';
import moment from 'moment';
import { Session } from 'meteor/session';
import { createContainer } from 'react-meteor-data';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import NumericInput from 'react-numeric-input';
import TimeInput from 'time-input';

export class BookingsListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startTime: this.props.booking.startTime,
            tourLength: this.props.booking.tourLength,
            name: this.props.booking.name,
            numberOfPeople: this.props.booking.numberOfPeople
        }
    }

    handleRemoval() {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this chauffeur!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, I didn't mean it!",
            closeOnConfirm: false,
            closeOnCancel: false
            },
            (isConfirm) => {
                if (isConfirm) {
                    swal("Deleted!", "The booking has been deleted.", "success");
                    this.props.call('bookings.remove', this.props.booking._id);
                } else {
                    swal("Cancelled", "Your booking is safe :)", "error");
                }
            });
    }

    handleUpdate(e) {
        let update = e.target.value;
        const key = e.target.name;
        if (key === 'tourLength' | key === 'numberOfPeople') {
            this.setState({
                [key]: Number(e.target.value)
            })
            this.props.call('bookings.update', this.props.booking._id, {
                [key]: Number(e.target.value)
            })
        } else {
            this.setState({
                [key]: e.target.value
            })
            this.props.call('bookings.update', this.props.booking._id, {
                [key]: e.target.value
            })
        }

    }

    handleNumberUpdate(e) {
        this.setState({
            tourLength: e
        })
        this.props.call('bookings.update', this.props.booking._id, {
            tourLength: e
        })
    }

    handlePeopleNumberUpdate(e) {
        this.setState({
            numberOfPeople: e
        })
        this.props.call('bookings.update', this.props.booking._id, {
            numberOfPeople: e
        })
    }

    handleTimeUpdate(e) {
        this.setState({
            startTime: e
        })
        this.props.call('bookings.update', this.props.booking._id, {
            startTime: e
        })
    }

    render() {
        return (
            <div>
                <h5>{ this.props.booking.startTime } - {this.props.booking.tourLength} minutes</h5>
                <TimeInput placeholder="Start Time" value={this.state.startTime} name="startTime" onChange={this.handleTimeUpdate.bind(this)}/>
                <NumericInput min={0} value={this.state.tourLength} name="tourLength" onChange={this.handleNumberUpdate.bind(this)}/>
                <p>Name booked under:</p>
                <input placeholder="Name" value={this.state.name} name="name" onChange={this.handleUpdate.bind(this)}/>
                <p>Number of people:</p>
                <NumericInput min={0} value={this.state.numberOfPeople} name="numberOfPeople" onChange={this.handlePeopleNumberUpdate.bind(this)}/>
                <button onClick={this.handleRemoval.bind(this)}>Remove</button>
                <p>-----------------------</p>
            </div>
        );
    }
}

BookingsListItem.propTypes = {
    booking: PropTypes.object.isRequired,
    call: PropTypes.func.isRequired
}

export default createContainer(() => {
    return {
        call: Meteor.call
    };
}, BookingsListItem);