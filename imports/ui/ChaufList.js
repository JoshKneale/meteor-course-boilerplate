import React from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'react-meteor-data';
import { Session } from 'meteor/session';
import PropTypes from 'prop-types';
import moment from 'moment';

import { Chaufs } from '../api/chaufs';
import ChaufListItem from './ChaufListItem';

export const ChaufList = (props) =>{

    return (
        <div className="item-list">
            {/*<BookingsListHeader/>*/}
            <h4>Chauffuers</h4>
            {props.chaufs.map( (chauf) => {
                return <ChaufListItem key={chauf._id} chauf={chauf}/>;
            })}
        </div>
    );
};

ChaufList.propTypes = {
    chaufs: PropTypes.array,
}

export default createContainer(() => {
    Meteor.subscribe('chaufs');

    return {
        chaufs: Chaufs.find({working: true}, {sort: {finishTime: 1}}).fetch()
    };
}, ChaufList)