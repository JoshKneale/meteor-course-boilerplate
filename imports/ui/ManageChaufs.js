import React from 'react';
import Modal from 'react-modal';
import { createContainer } from 'react-meteor-data';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import swal from 'sweetalert';

import { Chaufs } from '../api/chaufs';
import ManageChaufsListItem from './ManageChaufsListItem';

export class ManageChaufs extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            error: '',
            search: ''
        }
    }

    handleWorkingReset(){
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this chauffeur!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, reset the chauffeurs!",
            cancelButtonText: "No, I didn't mean it!",
            closeOnConfirm: false,
            closeOnCancel: false
            },
            (isConfirm) => {
                if (isConfirm) {
                    swal("Deleted!", "All chauffeurs reset.", "success");
                    this.props.call('chaufs.resetWork');
                } else {
                    swal("Cancelled", "Your chauffeurs are safe :)", "error");
                }
            });
    }

    handleSearch(e){
        const search = e.target.value;
        this.setState({search})
    }

    handleModalClose() {
        this.setState({
            isOpen: false, 
            error: ''})
    };

    render() {
        return (
            <div>
                <button className="button" onClick={ () => this.setState({isOpen: true}) }>Manage Chauffeurs</button>
                <Modal 
                isOpen={this.state.isOpen} 
                contentLabel="All Chauffeurs"
                onRequestClose={this.handleModalClose.bind(this)}
                className="boxed-view__box"
                overlayClassName="boxed-view boxed-view__modal"
                >
                <button className="button" onClick={()=> this.props.call('chaufs.insert')}>Add new chauffeur</button>
                <button className="button" onClick={this.handleWorkingReset.bind(this)}>Reset all working Chauffeurs</button>
                <div>
                    <input onChange={this.handleSearch.bind(this)} value={this.state.search} placeholder="Search"/>
                </div>
                {this.props.chaufs.map( (chauf) => {
                    if (chauf.name.toLowerCase().includes(this.state.search.toLowerCase()) || chauf.code.toLowerCase().includes(this.state.search.toLowerCase())) {
                        return <ManageChaufsListItem key={chauf._id} chauf={chauf}/>;
                    }
                })}
                </Modal>
            </div>
        )
    }
}

ManageChaufs.propTypes = {
    chaufs: PropTypes.array.isRequired,
    call: PropTypes.func
}

export default createContainer(() => {
    Meteor.subscribe('chaufs');

    return {
        chaufs: Chaufs.find({}).fetch(),
        call: Meteor.call
    };
}, ManageChaufs)