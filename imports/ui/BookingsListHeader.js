import React from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'react-meteor-data';
import { Session } from 'meteor/session';
import PropTypes from 'prop-types';

export const BookingsListHeader = (props) => {
    return (
        <div className="item-list__header">
            <button className="button" onClick={()=> props.meteorCall('bookings.insert')}>Add Booking</button>
        </div>
    );
};

BookingsListHeader.propTypes = {
    meteorCall: PropTypes.func.isRequired,
    Session: PropTypes.object.isRequired
}

export default createContainer(() => {
    return {
        meteorCall: Meteor.call,
        Session
    }
}, BookingsListHeader)