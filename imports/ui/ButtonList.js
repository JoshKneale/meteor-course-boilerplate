import React from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'react-meteor-data';
import PropTypes from 'prop-types';
import moment from 'moment';

import { Chaufs } from '../api/chaufs';

export class ButtonList extends React.Component{
    constructor(props) {
        super(props);   
    }

    nextTour() {
        // Ensure all onStation values are correct
        const working = Chaufs.find({ working: true }).fetch();
            working.forEach(function(chauf) {
                if (moment(chauf.returnTime).isBefore()) {
                    this.props.call('chaufs.update', { _id: chauf._id }, { $set: { onStation: true }})
                }
            })

        // is a chauffeur available now?
        const onStation = Chaufs.find({ onStation: true }).count()
        if(onStation > 0) {
            //if yes, set tour for 15 minutes
            alert('Number of staff on station = '+ onStation + '. Tour can be set!');
            console.log(Chaufs.find({ onStation: true }).fetch());
        } else {
            //if no, find next one back
            working.forEach(function(chauf) {
                //check return time of staff
                console.log(moment(chauf.returnTime).isAfter());
                //console.log(moment().subtract(moment(chauf.returnTime)).format('HH:mm'));
            }, this);
        }
    }

    render() {
        return (
            <div className="item-list">
                <button className="button" onClick={this.nextTour.bind(this)}>Next tour time available</button>
            </div>
        );
    }
};

ButtonList.propTypes = {
    call: PropTypes.func.isRequired,
    chaufs: PropTypes.array.isRequired
}

export default createContainer(() => {
    Meteor.subscribe('chaufs');
    return {
        call: Meteor.call,
        chaufs: Chaufs.find({working: true}).fetch()
    };
}, ButtonList);