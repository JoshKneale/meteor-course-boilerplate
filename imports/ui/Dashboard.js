import React from 'react';

import PrivateHeader from './PrivateHeader';
import BookingsList from './BookingsList';
import ChaufList from './ChaufList';
import ManageChaufs from './ManageChaufs';
import { ButtonList } from './ButtonList';

export default () => {
    return (
        <div>
            <PrivateHeader title="Tour Organiser"/>
            <div className="page-content">
                <div className="page-content__bookings">
                    <BookingsList/>
                </div>
                <div className="page-content__buttons">
                    <ButtonList/>
                </div>
                <div className="page-content__chaufs">
                    <ChaufList/>
                    <ManageChaufs/>
                </div>
            </div>
        </div>
    )
};