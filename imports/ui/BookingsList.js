import React from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'react-meteor-data';
import { Session } from 'meteor/session';
import PropTypes from 'prop-types';

import { Bookings } from '../api/bookings';
import BookingsListHeader from './BookingsListHeader';
import BookingsListItem from './BookingsListItem';

export const BookingsList = (props) => {

    return (
        <div className="item-list">
            <BookingsListHeader/>
            {props.bookings.map( (booking) => {
                return <BookingsListItem key={booking._id} booking={booking}/>;
            })}
        </div>
    );
};

BookingsList.propTypes = {
    bookings: PropTypes.array
}

export default createContainer(() => {
    Meteor.subscribe('bookings');

    return {
        bookings: Bookings.find({}, {sort: {startTime: 1}}).fetch()
    };
}, BookingsList)