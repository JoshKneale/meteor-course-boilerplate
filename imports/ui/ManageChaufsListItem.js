import React from 'react';
import moment from 'moment';
import { Session } from 'meteor/session';
import { createContainer } from 'react-meteor-data';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import NumericInput from 'react-numeric-input';
import swal from 'sweetalert';

export class ManageChaufsListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.chauf.name,
            code: this.props.chauf.code,
            tourLength: this.props.chauf.tourLength,
            working: this.props.chauf.working,
            startTime: this.props.chauf.startTime
        }
    }

    handleUpdate(e) {
        const key = e.target.name;
        if (key === 'tourLength') {
            this.setState({
                [key]: Number(e.target.value)
            })
            this.props.call('chaufs.update', this.props.chauf._id, {
                [key]: Number(e.target.value)
            })
        } else if (key === 'code') {
            this.setState({
                [key]: e.target.value.toUpperCase()
            })
            this.props.call('chaufs.update', this.props.chauf._id, {
                [key]: e.target.value.toUpperCase()
            })
        }else {
            this.setState({
                [key]: e.target.value
            })
            this.props.call('chaufs.update', this.props.chauf._id, {
                [key]: e.target.value
            })
        }

    }

    handleRemoval() {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this chauffeur!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, I didn't mean it!",
            closeOnConfirm: false,
            closeOnCancel: false
            },
            (isConfirm) => {
                if (isConfirm) {
                    swal("Deleted!", "The chauffeur has been deleted.", "success");
                    this.props.call('chaufs.remove', this.props.chauf._id);
                    //this.props.browserHistory.push('/dashboard');
                } else {
                    swal("Cancelled", "Your chauffeur is safe :)", "error");
                }
            });
    }

    toggleWorking(){
        this.props.call('chaufs.update', this.props.chauf._id, {
                working: !this.props.chauf.working
        })
    }

    handleNumberUpdate(e) {
        this.setState({
            tourLength: e
        })
        this.props.call('chaufs.update', this.props.chauf._id, {
            tourLength: e
        })
    }

    render() {
        return (
            <div>
                <input placeholder="Name" value={this.state.name} name="name" onChange={this.handleUpdate.bind(this)}/>
                <input placeholder="Code" value={this.state.code} name="code" onChange={this.handleUpdate.bind(this)}/>
                <NumericInput min={0} placeholder="Average Tour Length" value={this.state.tourLength} name="tourLength" onChange={this.handleNumberUpdate.bind(this)}/>
                <input placeholder="Start Time" value={this.state.startTime} name="startTime" onChange={this.handleUpdate.bind(this)}/>
                <button onClick={this.toggleWorking.bind(this)}>{this.props.chauf.working ? 'Working' : 'Not Working'}</button>
                <button onClick={this.handleRemoval.bind(this)}>Remove</button>
                <p>-----------------------</p>
            </div>
        );
    }
}

ManageChaufsListItem.propTypes = {
    chauf: PropTypes.object.isRequired,
    call: PropTypes.func.isRequired
}

export default createContainer(() => {
    return {
        call: Meteor.call
    };
}, ManageChaufsListItem);