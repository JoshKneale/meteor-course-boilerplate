import React from 'react';
import moment from 'moment';
import { Session } from 'meteor/session';
import { createContainer } from 'react-meteor-data';
import PropTypes from 'prop-types';

export class ChaufListItem extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            color: ''
        }
    }

    componentDidMount(){
        this.timer = setInterval(this.colorDiv.bind(this), 60000);
        this.colorDiv();
    }

    componentWillUnmount(){
        clearInterval(this.timer);
    }

    colorDiv() {
        if (moment(this.props.chauf.returnTime).isAfter()){
            this.setState({ color: 'color-red'});
        } else {
            this.setState({ color: ''});
        }
    }

    sendOnTour() {
        this.setState({ color: 'color-red'});
        this.props.call('chaufs.update', this.props.chauf._id, {
                returnTime: moment().add(this.props.chauf.tourLength, 'm').format(),
                onStation: false
        })
    }

    cancelTour() {
        this.setState({ color: ''});
        this.props.call('chaufs.update', this.props.chauf._id, {
                returnTime: '',
                onStation: true
        })
    }

    render() {
        return (
            <div className={this.state.color}>
                <p>{this.props.chauf.code}</p>
                <button onClick={this.sendOnTour.bind(this)}>Send on Tour</button>
                <button onClick={this.cancelTour.bind(this)}>Cancel Tour</button>
                <p>{moment(this.props.chauf.returnTime).isAfter() ? 'Return Time: ' + moment(this.props.chauf.returnTime).format('HH:mm') : undefined}</p>
            </div>
        )
    }
}

ChaufListItem.propTypes = {
    chauf: PropTypes.object.isRequired,
    Session: PropTypes.object.isRequired,
    call: PropTypes.func.isRequired
}

export default createContainer(() => {
    return { Session,
        call: Meteor.call
 };
}, ChaufListItem);