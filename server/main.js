import { Meteor } from 'meteor/meteor';
import { WebApp } from 'meteor/webapp';

import '../imports/api/users';
import '../imports/api/bookings';
import '../imports/api/chaufs';
import '../imports/startup/simple-schema-configuration.js';

Meteor.startup(() => {

});
